using Test
using NeuronToolbox

@testset "Models" begin
    @testset "Morris-Lecar" begin
        ml_params = Models.MorrisLecarUtils.example_parameters()
        @test typeof(ml_params) <: Vector

        ml_model = Models.MorrisLecar(ml_params)
        @test typeof(ml_model) <: Models.AbstractModel

        @test ml_model(ones(2)) == [-465.05509672144603, -0.050839673019188215]
    end

    @testset "Interneuron" begin
        interneuron_params = Models.InterneuronUtils.example_parameters()
        @test typeof(interneuron_params) <: Vector

        interneuron_model = Models.Interneuron(interneuron_params)
        @test typeof(interneuron_model) <: Models.AbstractModel

        @test interneuron_model(ones(3)) == [-650.6861292175236, -1.6567348002477154, -0.16455948110508345]
    end

    @testset "FitzHugh-Nagumo" begin
        fhn_params = Models.FitzHughNagumoUtils.example_parameters()
        @test typeof(fhn_params) <: Vector

        fhn_model = Models.FitzHughNagumo(fhn_params)
        @test typeof(fhn_model) <: Models.AbstractModel

        @test fhn_model(ones(2)) == [0.4666666666666668, 0.072]
    end

    # TODO: Add Hodgkin-Huxley test set
end

@testset "Couplings" begin
    strength = 1
    first_var_diff = Couplings.FirstVarDifference(strength)

    @test typeof(first_var_diff) <: Couplings.AbstractCoupling

    args = ([0, 0], [1, 1], [[2, 2], [3, 3]], 0)
    answer = [-3, 0]
    @test first_var_diff(args...) == answer
end

@testset "Networks" begin
    no_coupling = (ẋ, x, y, t; kwargs...) -> ẋ
    strength = 1
    coupling = Couplings.FirstVarDifference(strength)

    # INFO: Only for one model, others *should* be the same
    model = Models.FitzHughNagumo(Models.FitzHughNagumoUtils.example_parameters())
    constant = Models.ConstantModel()

    separateModels = Networks.CoupledPair(model, no_coupling)

    @test typeof(separateModels) <: Networks.AbstractNetwork
    @test separateModels([ones(2), ones(2)]) == [model(ones(2)), model(ones(2))]

    separateModels_split = Networks.CoupledPair(model, no_coupling; split_vars=[1:2, 3:4])

    @test typeof(separateModels_split) <: Networks.AbstractNetwork
    @test separateModels_split(ones(4)) == [model(ones(2))..., model(ones(2))...]

    coupledPair = Networks.CoupledPair(constant, coupling; split_vars=[1:2, 3:4])
    args_with_0 = [1, 0, 2, 0]
    args_without_0 = [1, 1, 2, 2]

    @test coupledPair(args_without_0) == [-1, 0, 1, 0]
    @test coupledPair(args_without_0) == coupledPair(args_with_0)

    # INFO: Just a 2x2 model + no_coupling example
    separateMatrix = Networks.CoupledMatrix(
        [model model; model model],
        [no_coupling no_coupling; no_coupling no_coupling]
    )

    @test typeof(separateMatrix) <: Networks.AbstractNetwork

    args = [ones(2), ones(2), ones(2), ones(2)]
    answer = [[0.4666666666666668, 0.072], [0.4666666666666668, 0.072], [0.4666666666666668, 0.072], [0.4666666666666668, 0.072]]
    @test separateMatrix(args) == answer
    @test separateMatrix(reshape(args, (2, 2))) == reshape(answer, (2, 2))

    separateMatrix_split = Networks.CoupledMatrix(
        [model model; model model],
        [no_coupling no_coupling; no_coupling no_coupling],
        split_vars=[1:2, 3:4, 5:6, 7:8]
    )

    @test typeof(separateMatrix_split) <: Networks.AbstractNetwork
    @test separateMatrix_split(ones(8)) == [0.4666666666666668, 0.072, 0.4666666666666668, 0.072, 0.4666666666666668, 0.072, 0.4666666666666668, 0.072]

    coupledMatrix = Networks.CoupledMatrix(
        [constant constant; constant constant],
        [coupling coupling; coupling coupling],
        split_vars=[1:2, 3:4, 5:6, 7:8]
    )
    args_with_0 = [1, 0, 2, 0, 3, 0, 4, 0]
    args_without_0 = [1, 1, 2, 2, 3, 3, 4, 4]

    @test coupledMatrix(args_without_0) == [-6, 0, -2, 0, 2, 0, 6, 0]
    @test coupledMatrix(args_without_0) == coupledMatrix(args_with_0)

    # INFO: GenericNetwork should be typically used for more unusual cases, but as an example...

    network_no_couple = (dxy, xy, t; kwargs...) -> dxy

    network = Networks.GenericNetwork(
        [model model; model model],
        network_no_couple
    )
    network_split = Networks.GenericNetwork(
        [model model; model model],
        network_no_couple,
        split_vars=[1:2, 3:4, 5:6, 7:8]
    )

    @test network(args) == separateMatrix(args)
    @test network(reshape(args, (2, 2))) == separateMatrix(reshape(args, (2, 2)))
    @test network_split(ones(8)) == separateMatrix_split(ones(8))

    strong_fvd = Couplings.FirstVarDifference(2)
    weak_fvd = Couplings.FirstVarDifference(1)

    adjacencyMatrix = Networks.AdjacencyMatrix([constant, constant, constant],
        [
            no_coupling strong_fvd weak_fvd
            no_coupling no_coupling no_coupling
            no_coupling no_coupling no_coupling
        ]; split_vars=[1:2, 3:4, 5:6])

    adjacencyMatrix_diag = Networks.AdjacencyMatrix([constant, constant, constant],
        [
            strong_fvd strong_fvd weak_fvd
            no_coupling strong_fvd no_coupling
            no_coupling no_coupling strong_fvd
        ]; split_vars=[1:2, 3:4, 5:6])

    @test adjacencyMatrix([1, 1, 3, 3, 4, 4]) == [0, 0, 4, 0, 3, 0]
    @test adjacencyMatrix_diag([1, 1, 3, 3, 4, 4]) == adjacencyMatrix([1, 1, 3, 3, 4, 4])
    @test adjacencyMatrix_diag([1, 0, 3, 0, 4, 0]) == adjacencyMatrix_diag([1, 1, 3, 3, 4, 4])
end
