# NeuronToolbox.jl

NeuronToolbox.jl is a Julia package to provide various neurological models and methods of organizing them into networks

> Migrated from monorepo now located at https://gitlab.com/sceptri-university/gamu/dynamical-sandbox