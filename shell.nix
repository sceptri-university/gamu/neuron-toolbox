let
  pkgs = import <nixpkgs> {};
  unstablePkgs = import <nixos-unstable> {};
  lib = import <lib> {};
  config = import <config> {};
  configPath = builtins.getEnv "NIXOS_CONFIGURATION_DIR" + "/.";
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      (unstablePkgs.julia.withPackages
        [
          "Test"
          "DifferentialEquations"
        ])
      gnumake
    ];
    shellHook = ''
      echo "Welcome to NeuronToolbox.jl shell"
    '';
  }
