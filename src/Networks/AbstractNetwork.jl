import Base.Callable
using ..Models

const MatOrNone = Union{VecOrMat,Nothing}
const VariablesType = Union{Array{<:Real},Array{<:Array{<:Real}}}

function feed(models::VecOrMat{<:Callable}, variables::VariablesType; split_vars::MatOrNone, kwargs...)
    return isnothing(split_vars) ?
           map(enumerate(variables)) do args
        index, vars = args
        return models[index](vars; kwargs...)
    end : map(enumerate(split_vars)) do args
        index, singular = args
        return models[index](variables[singular]; kwargs...)
    end
end

feed(func::Callable, variables::VariablesType; kwargs...) = feed(fill(func, size(variables)),
    variables; kwargs...)

split(variables::VariablesType; kwargs...) = feed(identity, variables; kwargs...)

abstract type AbstractNetwork <: Function end

(::AbstractNetwork)(variables::VariablesType, params, t; kwargs...) = error("Not implemented!")

struct GenericNetwork <: AbstractNetwork
    models::VecOrMat{<:AbstractModel}
    func::Callable
    split_vars::MatOrNone
end

GenericNetwork(models, func; split_vars=nothing) =
    GenericNetwork(models, func, split_vars)

function (network::GenericNetwork)(variables::VariablesType, params=Real[], time=0.0; kwargs...)
    coupled = network.func(feed(network.models, variables; split_vars=network.split_vars, kwargs...),
        variables, time;
        split_vars=network.split_vars, kwargs...)

    return isnothing(network.split_vars) ?
           reshape(coupled, size(variables)) : collect(Iterators.flatten(coupled))
end
