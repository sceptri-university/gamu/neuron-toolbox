module Networks

using InvertedIndices

include("AbstractNetwork.jl")
include("CoupledNetworks.jl")
include("AdjacencyNetworks.jl")

export AbstractNetwork, GenericNetwork
export CoupledPair, CoupledMatrix
export AdjacencyMatrix

end
