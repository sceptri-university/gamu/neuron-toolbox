struct AdjacencyMatrix <: AbstractNetwork
    models::Vector{<:AbstractModel}
    couplings::Matrix{<:Callable}
    split_vars::MatOrNone
end

AdjacencyMatrix(models, couplings; split_vars=nothing) = AdjacencyMatrix(models, couplings, split_vars)

function (coupling::AdjacencyMatrix)(variables::VariablesType, params=Real[], time=0.0; kwargs...)
    derivatives = feed(coupling.models, variables;
        split_vars=coupling.split_vars, kwargs...)

    coupled = copy(derivatives)
    vars = split(variables; split_vars=coupling.split_vars)

    for (col_index, column) in enumerate(eachcol(coupling.couplings))
        for (row_index, singular_coupling) in enumerate(column)
            if row_index == col_index
                continue
            end

            coupled[col_index] = singular_coupling(coupled[col_index], vars[col_index],
                vars[row_index], time)
        end
    end

    # TODO: Add update! everywhere

    return isnothing(coupling.split_vars) ?
           reshape(coupled, size(variables)) : collect(Iterators.flatten(coupled))
end
