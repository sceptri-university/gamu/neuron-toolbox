struct CoupledPair <: AbstractNetwork
    left_model::AbstractModel
    right_model::AbstractModel
    left_coupling::Callable
    right_coupling::Callable
    split_vars::MatOrNone
end

CoupledPair(left_model::AbstractModel, right_model::AbstractModel, coupling::Callable;
    split_vars::MatOrNone=nothing) =
    CoupledPair(left_model, right_model, coupling, coupling, split_vars)

CoupledPair(model::AbstractModel, coupling::Callable; split_vars::MatOrNone=nothing) =
    CoupledPair(model, model, coupling, coupling, split_vars)

function (couple::CoupledPair)(variables::VariablesType, params=Real[], time=0.0; kwargs...)
    derivatives = feed([couple.left_model, couple.right_model], variables;
        split_vars=couple.split_vars, kwargs...)

    vars = split(variables; split_vars=couple.split_vars)
    coupled = [
        couple.left_coupling(derivatives[1], vars..., time; kwargs...),
        couple.right_coupling(derivatives[2], reverse(vars)..., time; kwargs...),
    ]

    # TODO: Add update! everywhere

    return isnothing(couple.split_vars) ?
           coupled : collect(Iterators.flatten(coupled))
end

struct CoupledMatrix <: AbstractNetwork
    models::Matrix{<:AbstractModel}
    couplings::Matrix{<:Callable}
    split_vars::MatOrNone
end

CoupledMatrix(models, couplings; split_vars=nothing) = CoupledMatrix(models, couplings, split_vars)

function (coupling::CoupledMatrix)(variables::VariablesType, params=Real[], time=0.0; kwargs...)
    derivatives = feed(coupling.models, variables;
        split_vars=coupling.split_vars, kwargs...)

    vars = split(variables; split_vars=coupling.split_vars)
    coupled = map(enumerate(derivatives)) do args
        index, derivative = args
        return coupling.couplings[index](derivative, vars[index], vars[Not(index)], time; kwargs...)
    end

    # TODO: Add update! everywhere

    return isnothing(coupling.split_vars) ?
           reshape(coupled, size(variables)) : collect(Iterators.flatten(coupled))
end
