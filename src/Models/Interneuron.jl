module InterneuronUtils

V̇(V, h, n, C, I, gL, EL, gNa, ENa, gK, EK) = 1 / C * (I - gL * (V - EL) - gNa / (1 + exp(-.8e-1 * V - 2.08))^3 * h * (V - ENa) - gK * n^4 * (V - EK))
ḣ(V, h, n, C, I, gL, EL, gNa, ENa, gK, EK) = 1.666666667 * (1 / (1 + exp(0.13 * V + 4.94)) - h) * (1 + exp(-0.12 * V - 8.04))
ṅ(V, h, n, C, I, gL, EL, gNa, ENa, gK, EK) = (1 / (1 + exp(-.45e-1 * V - 0.450)) - n) / (0.5 + 2 / (1 + exp(.45e-1 * V - 2.250)))

function derivative(variables, parameters; kwargs...)
    arguments = [variables..., parameters...]
    return [V̇(arguments...), ḣ(arguments...), ṅ(arguments...)]
end

example_parameters() = [1, 24, 0.1, -60, 30, 45, 20, -80]
end

Interneuron(parameters::Vector{<:Real}) =
    GenericModel(InterneuronUtils.derivative, parameters)
