module MorrisLecarUtils
mᵢ(V, β₁, β₂) = 1 / 2 * (1 + tanh((V - β₁) / β₂))
nᵢ(V, β₃, β₄) = 1 / 2 * (1 + tanh((V - β₃) / β₄))
τₙ(V, β₃, β₄) = 1 / cosh((V - β₃) / (2 * β₄))

V̇(V, n, C, gₖ, Vₖ, gₗ, Vₗ, gₐ, Vₐ, β₁, β₂, β₃, β₄, φ, Iₑ) = 1 / C * (Iₑ - gₗ * (V - Vₗ) - gₐ * mᵢ(V, β₁, β₂) * (V - Vₐ) - gₖ * n * (V - Vₖ))
ṅ(V, n, C, gₖ, Vₖ, gₗ, Vₗ, gₐ, Vₐ, β₁, β₂, β₃, β₄, φ, Iₑ) = φ * (nᵢ(V, β₃, β₄) - n) / τₙ(V, β₃, β₄)

function derivative(variables, parameters; kwargs...)
    arguments = [variables..., parameters...]
    return [V̇(arguments...), ṅ(arguments...)]
end

example_parameters() = [1, 8, -80, 2, -60, 4, 120, -1.2, 18, 10, 17.4, 1 / 15, 38]
end

MorrisLecar(parameters::Vector{<:Real}) =
    GenericModel(MorrisLecarUtils.derivative, parameters)
