import Base.Callable

abstract type AbstractModel <: Function end

struct GenericModel <: AbstractModel
    func::Callable
    parameters::Vector{<:Real}
end

(::AbstractModel)(variables; kwargs...) = error("Not implemented!")
(model::GenericModel)(variables; kwargs...) = model.func(variables, model.parameters; kwargs...)

ConstantModel(p::Vector{<:Real}=Float64[]) = GenericModel((x, p; kwargs...) -> zeros(size(x)), p)
