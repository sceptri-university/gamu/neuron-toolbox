module FitzHughNagumoUtils
V̇(V, W, a, b, c, d, iₑ) = V - V^3 / 3 - W + iₑ
Ẇ(V, W, a, b, c, d, iₑ) = a * (b * V - c * W + d)

function derivative(variables, parameters; kwargs...)
    arguments = [variables..., parameters...]
    return [V̇(arguments...), Ẇ(arguments...)]
end

example_parameters() = [0.08, 1.0, 0.8, 0.7, 0.8]
end

FitzHughNagumo(parameters::Vector{<:Real}) =
    GenericModel(FitzHughNagumoUtils.derivative, parameters)
