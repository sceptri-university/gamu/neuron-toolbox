module Models

include("AbstractModel.jl")
include("MorrisLecar.jl")
include("Interneuron.jl")
include("FitzHughNagumo.jl")
include("HodgkinHuxley.jl")

export AbstractModel, GenericModel
export MorrisLecar, Interneuron, FitzHughNagumo, HodgkinHuxley
export ConstantModel

end
