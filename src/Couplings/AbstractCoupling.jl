abstract type AbstractCoupling <: Function end

struct None <: AbstractCoupling end
(coupling::None)(derivative, coords; kwargs...) = derivative

(::AbstractCoupling)(args...; kwargs...) = error("Not implemented!")
update!(::AbstractCoupling, args...) = @info("Not applicable!")

abstract type SynchronousCoupling <: AbstractCoupling end

correct_effector(::SynchronousCoupling) =
    x -> typeof(x) <: Array{<:Array{<:Real}} ? x : [x]

struct FirstVarDifference <: SynchronousCoupling
    strengh::Real
end

function (coupling::FirstVarDifference)(d_effectee, effectee, effectors, time;
    correction=correct_effector(coupling), kwargs...)

    effectors = correction(effectors)

    coupling_effect = zeros(size(d_effectee))
    coupling_effect[1] = sum(effectee[1] - one_effector[1] for one_effector in effectors)

    return d_effectee + coupling.strengh * coupling_effect
end

abstract type AsynchronousCoupling <: AbstractCoupling end

mutable struct OneStepDelayed <: AsynchronousCoupling
    coupling::SynchronousCoupling
    delayed::VecOrMat{<:Real}
end

correct_effector(coupling::OneStepDelayed, indices) =
    _ -> correct_effector(coupling.coupling)(coupling.delayed[indices])

(delayed::OneStepDelayed)(args...; effector_indices, kwargs...) =
    delayed.coupling(args...; correction=correct_effector(delayed, effector_indices), kwargs...)

function update!(coupling::OneStepDelayed, delay)
    coupling.delayed .= delay
end
